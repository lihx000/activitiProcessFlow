package com.gaul.activiti;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.ProcessEngines;

/**
 * 工作流引擎factory
 */
public class ProcessEngineFactory {
	/**
	 * 创建工作流引擎
	 */
	public static ProcessEngine createProcessEngine() {
		/*ProcessEngineConfiguration config = ProcessEngineConfiguration.createStandaloneProcessEngineConfiguration();
		//设置数据库连接属性
		config.setJdbcDriver("com.mysql.jdbc.Driver");
		config.setJdbcUrl("jdbc:mysql://localhost:3306/activiti?createDatabaseIfNotExist=true&useUnicode=true&characterEncoding=utf8");
		config.setJdbcUsername("root");
		config.setJdbcPassword("root");
		//创建数据表策略
		//DB_SCHEMA_UPDATE_FALSE="false" 不会自动创建表，没有表，则抛异常
		//DB_SCHEMA_UPDATE_CREATE_DROP="create-drop" 先删除，再创建表
		//DB_SCHEMA_UPDATE_TRUE="true" 假如没有表，则自动创建
		config.setDatabaseSchemaUpdate("true");
		//通过config对象创建ProcessEngine
		ProcessEngine engine = config.buildProcessEngine();*/
		
		/*ProcessEngineConfiguration config = ProcessEngineConfiguration.createProcessEngineConfigurationFromResource("activiti.cfg.xml");
		ProcessEngine engine = config.buildProcessEngine();*/
		
		//默认加载类路径下的activiti.cfg.xml
		ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
		System.out.println("流程引擎创建成功");
		return engine;
	}
}
