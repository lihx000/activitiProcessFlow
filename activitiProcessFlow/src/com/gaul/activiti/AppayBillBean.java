package com.gaul.activiti;

import java.io.Serializable;
import java.util.Date;

public class AppayBillBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer cost;
	private String appayPerson;
	private Date date;
	
	public AppayBillBean() {
		super();
	}
	public AppayBillBean(Integer id, Integer cost, String appayPerson, Date date) {
		super();
		this.id = id;
		this.cost = cost;
		this.appayPerson = appayPerson;
		this.date = date;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCost() {
		return cost;
	}
	public void setCost(Integer cost) {
		this.cost = cost;
	}
	public String getAppayPerson() {
		return appayPerson;
	}
	public void setAppayPerson(String appayPerson) {
		this.appayPerson = appayPerson;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
}
